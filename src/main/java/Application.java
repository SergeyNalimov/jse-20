import shape.Circle;
import shape.Rectangle;
import shape.Shape;
import shape.Square;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;

    public class Application {

        public static void main(String[] args) {
            final Collection<Circle> circles = new ArrayList<>();
            final Collection<Rectangle> rectangles = new ArrayList<>();
            final Collection<Square> square = new ArrayList<>();
            final Collection<Shape> shape = new ArrayList<>();

            addShape(circles, new Circle(9.0), new Circle(3.0));
            addShape(rectangles, new Rectangle(9.0, 3.0), new Rectangle(6.0, 3.0));
            addShape(square, new Square(6.0), new Square(4.0));
            addShape(shape, new Circle(8.0), new Rectangle(8.0, 2.0), new Square(7.0));


            System.out.println(getShapeCollectionArea(circles));
            System.out.println(getShapeCollectionArea(rectangles));
            System.out.println(getShapeCollectionArea(square));
            System.out.println(getShapeCollectionArea(shape));

        }

        private static <T extends Shape> void addShape(final Collection<T> shapeCollection, final T ... shapes) {
            shapeCollection.addAll(Arrays.asList(shapes));
        }

        private static <T extends Shape> Double getShapeCollectionArea(final Collection<T> shapeCollection) {
            Double area = 0.0;
            for (T shape: shapeCollection){
                area = area + shape.getArea();
            }
            return area;
        }

    }
