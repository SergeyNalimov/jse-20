package shape;

public class Rectangle extends Shape{

    private Double sideSizeA;

    private Double sideSizeB;

    public Rectangle(Double sideSizeA, Double sideSizeB) {
        this.sideSizeA = sideSizeA;
        this.sideSizeB = sideSizeB;
    }

    public Double getSideSizeA() {
        return sideSizeA;
    }

    public void setSideSizeA(Double sideSizeA) {
        this.sideSizeA = sideSizeA;
    }

    public Double getSideSizeB() {
        return sideSizeB;
    }

    public void setSideSizeB(Double sideSizeB) {
        this.sideSizeB = sideSizeB;
    }

    @Override
    public Double getArea() {
        return sideSizeA * sideSizeB;
    }
}
