package shape;

public abstract class Shape {

    public abstract Double getArea();

}
